/*
* @Author: luck-cui
* @Date:   2017-10-23 15:28:54
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-23 16:12:51
*/
import * as types from '../constants/ActionTypes'

export const addArticle = text => ({ type: types.ADD_ARTICLE, text })
export const deleteArticle = id => ({ type: types.DELETE_ARTICLE, id })
export const editArticle = (id, text) => ({ type: types.EDIT_ARTICLE, id, text })

