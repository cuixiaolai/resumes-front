/*
* @Author: luck-cui
* @Date:   2017-10-31 21:11:10
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-31 21:11:21
*/
 
module.exports = {
    host: 'localhost',
    port: 3011
}
