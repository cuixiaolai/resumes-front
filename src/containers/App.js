/*
* @Author: luck-cui
* @Date:   2017-10-23 15:16:57
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-31 23:12:08
*/
import React from 'react'
import { Router, Route } from 'react-router'
import Home from './Home'
import Show from './Show'
import NoResult from './NoResult'
import '../styles/normalize.css'
import '../styles/app.css'



const App = ({history}) => (
	<Router history={history}>
	  <Route  path="/" component={Show}/>
	  <Route  path="/add" component={Home}/>
	  <Route  path="/show/:id" component={Show}/>
	  <Route path="*" component={NoResult}/>
	</Router>
)


export default App
