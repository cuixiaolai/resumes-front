/*
* @Author: luck-cui
* @Date:   2017-10-26 14:08:11
* @Last Modified by:   xiaoxiaolai
* @Last Modified time: 2017-11-01 21:33:41
*/

import axios from 'axios';

//封装好的get和post接口，调用方法情况action文件
const instance = axios.create({
    baseURL: process.env.NODE_ENV === 'production' ? 'https://resumes.gkzyk.com/api/v1/' : 'http://localhost:3001/api/v1/', //设置默认api路径
    timeout: 5000, //设置超时时间
    headers: {'X-Custom-Header': 'foobar'},
});

export default instance;