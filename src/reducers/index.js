/*
* @Author: luck-cui
* @Date:   2017-10-23 15:24:49
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 14:24:21
*/
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import article from './article'
import resumes from './resumes'

const rootReducer = combineReducers({
  article,
  resumes,
  routing:routerReducer
  
})

export default rootReducer
