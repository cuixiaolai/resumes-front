/*
* @Author: luck-cui
* @Date:   2017-10-23 18:42:51
* @Last Modified by:   xiaoxiaolai
* @Last Modified time: 2017-11-01 21:37:13
*/

import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { browserHistory} from 'react-router'
import Remarkable from 'remarkable'
import hljs from 'highlight.js'
import * as resumesActions from '../actions/resumes'


class Home extends React.Component {
  constructor(props) {
    super(props)
      this.state={
        text:this.props.text.content||''
      }
    this.md= new Remarkable('full',{
      html:         true,        // Enable html tags in source
      xhtmlOut:     false,        // Use '/' to close single tags (<br />)
      breaks:       false,        // Convert '\n' in paragraphs into <br>
      langPrefix:   'language-',  // CSS language prefix for fenced blocks
      linkify:      true,        // Autoconvert url-like texts to links
      typographer:  true,        // Enable smartypants and other sweet transforms
      linkTarget:   '', 
      quotes: '“”‘’',
      highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
          try {
            return hljs.highlight(lang, str).value;
          } catch (err) {}
        }
        try {
          return hljs.highlightAuto(str).value;
        } catch (err) {}
        return ''; // use external default escaping
      }
    });

    this.md.renderer.rules.table_open = function () {
      return '<table class="table table-striped">\n';
    };

    this.md.renderer.rules.paragraph_open = function (tokens, idx) {
      var line;
      if (tokens[idx].lines && tokens[idx].level === 0) {
        line = tokens[idx].lines[0];
        return '<p class="line" data-line="' + line + '">';
      }
      return '<p>';
    };

    this.md.renderer.rules.heading_open = function (tokens, idx) {
      var line;
      if (tokens[idx].lines && tokens[idx].level === 0) {
        line = tokens[idx].lines[0];
        return '<h' + tokens[idx].hLevel + ' class="line" data-line="' + line + '">';
      }
      return '<h' + tokens[idx].hLevel + '>';
    };
  }

  handler(e){
    this.setState({
      text:e.target.value
    })
  }

  rawMarkup(){
    var rawMarkup = this.md.render(this.state.text);
    return {__html:rawMarkup};
  }
  
  submit(e){
    this.props.postResumes(this.state.text)
  }
  componentWillReceiveProps(nextProps){
    if(!nextProps.isFailed&&nextProps.text._id) {
      const id = nextProps.text._id
      const path = `/show/${id}`
      browserHistory.push(path)
    }
  }
  render(){
    return (
      <div className='body'>
        <div className='flex'>
        <textarea 
          value = {this.state.text} 
          className='show half' 
          onChange={this.handler.bind(this)} />
        <section 
          className='show half'
          >
        <div className='full-height' dangerouslySetInnerHTML={this.rawMarkup()}>
        </div>
        </section>
        </div>
        <div className='sbumitButton'>
          <button type="button" className="btn btn-default ten" onClick={this.submit.bind(this)}>提交</button>
        </div>
        <div className='sbumitButton'>
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  ...state.resumes
})

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(resumesActions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)

