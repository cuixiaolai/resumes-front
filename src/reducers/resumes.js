/*
* @Author: luck-cui
* @Date:   2017-10-26 13:47:10
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-31 16:15:32
*/

import { IS_AESUMES_UPLOAD, ADD_AESUMES_SUCCESS, ADD_AESUMES_FAILD, DELETE_AESUMES, GET_AESUMES_SUCCESS, GET_AESUMES_FAILD } from '../constants/ActionTypes'

const initialState = 
  {
    text: {},
    isUploading:false,
    isFailed:false,
  }


export default function articles(state = initialState, action) {
  switch (action.type) {

    case IS_AESUMES_UPLOAD:
      return {
      	...state,
      	isUploading:true
      }

    case ADD_AESUMES_SUCCESS:
      return {
      	...state,
        text:action.text,
      	isUploading:false,
      	isFailed:false,
      }

    case ADD_AESUMES_FAILD:
      return {
        ...state,
        isUploading:false,
        isFailed:true,
      }

    case DELETE_AESUMES:
      return {
        ...state,
        text:{},
        isUploading:false,
        isFailed:false,
      }

    case GET_AESUMES_SUCCESS:
      return {
        ...state,
        text:action.text[0]||{},
        isUploading:false,
        isFailed:false,
      }

    case GET_AESUMES_FAILD:
      return {
      	...state,
        text:{},
      	isUploading:false,
      	isFailed:true,
      }

    default:
      return state
  }
}
