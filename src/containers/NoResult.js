/*
* @Author: luck-cui
* @Date:   2017-10-23 18:52:04
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-31 23:43:53
*/


import React from 'react'

const NoResult = () => (
  <div className='center'>
    404 page
  </div>
)


export default NoResult
