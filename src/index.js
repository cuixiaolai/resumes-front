/*
* @Author: luck-cui
* @Date:   2017-10-23 15:07:29
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-31 23:25:51
*/
import "babel-polyfill";
import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import { browserHistory } from 'react-router'
import { routerMiddleware, syncHistoryWithStore } from 'react-router-redux'
import thunk from 'redux-thunk'


import { AppContainer } from 'react-hot-loader'


import App from './containers/App'
import reducer from './reducers'

// import { createStore, combineReducers, applyMiddleware } from 'redux';
// import { routerMiddleware, push } from 'react-router-redux'
const middlewares = [ thunk ]
// // Apply the middleware to the store
const middleware = routerMiddleware(browserHistory)

middlewares.push(middleware)
// const store = createStore(
//   reducers,
//   applyMiddleware(middleware)
// )
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
// const store = createStore(
  // reducer,
  applyMiddleware(...middlewares)
))

const history = syncHistoryWithStore(browserHistory, store)

render(
<AppContainer>
  <Provider store={store}>
	<App history={history}/>
  </Provider>
</AppContainer>
  ,
  document.getElementById('root') 
)

if(module.hot) {
    module.hot.accept('./containers/App', () => {
        const NextRootContainer = require('./containers/App').default
		render(
		<AppContainer>
		  <Provider store={store}>
			<NextRootContainer history={history}/>
		  </Provider>
		</AppContainer>
		  ,
		  document.getElementById('root') 
		)

    })
}
