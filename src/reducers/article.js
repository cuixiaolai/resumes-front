/*
* @Author: luck-cui
* @Date:   2017-10-23 15:45:15
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-23 15:52:36
*/
import { ADD_ARTICLE, DELETE_ARTICLE, EDIT_ARTICLE } from '../constants/ActionTypes'

const initialState = [
  {
    text: 'Use Redux',
    completed: false,
    id: 0
  }
]

export default function articles(state = initialState, action) {
  switch (action.type) {
    case ADD_ARTICLE:
      return [
        ...state,
        {
          id: state.reduce((maxId, article) => Math.max(article.id, maxId), -1) + 1,
          completed: false,
          text: action.text
        }
      ]

    case DELETE_ARTICLE:
      return state.filter(article =>
        article.id !== action.id
      )

    case EDIT_ARTICLE:
      return state.map(article =>
        article.id === action.id ?
          { ...article, text: action.text } :
          article
      )

    default:
      return state
  }
}
