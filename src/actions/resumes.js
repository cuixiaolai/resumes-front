/*
* @Author: luck-cui
* @Date:   2017-10-26 13:44:04
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-31 16:09:18
*/

import * as types from '../constants/ActionTypes'
import instance from '../utils/instance'

export const postResumes = text => dispatch =>{
	                              dispatch(isResumesUpload())
	                              return instance.post('resumes',{content:text})
	                                             .then(r=>{
	                                             	console.log('r',r)
	                                             	if(r.status === 200){
	                                             		dispatch(postResumesSuccess(r.data))
	                                             	}else{
	                                             		dispatch(postResumesFailed(r.data))
	                                             	}
	                                             	
	                                             })
	                                             .catch(e=>{
	                                             	console.log('e',e)
	                                             	dispatch(postResumesFailed(e))
	                                             })
	                            }

export const postResumesSuccess = text => ({ type: types.ADD_AESUMES_SUCCESS, text })
export const postResumesFailed = err => ({ type: types.ADD_AESUMES_FAILD, err })


export const getResumes = id => dispatch =>{
	                              return instance.get(`/resumes/${id}`)
	                                             .then(r=>{
	                                             	if(r.status === 200){
	                                             		dispatch(getResumesSuccess(r.data))
	                                             	}else{
	                                             		dispatch(getResumesFailed(r.data))
	                                             	}
	                                             	
	                                             })
	                                             .catch(e=>{
	                                             	dispatch(getResumesFailed(e))
	                                             })
	                            }

export const getResumesSuccess = text => ({ type: types.GET_AESUMES_SUCCESS, text })
export const getResumesFailed = err => ({ type: types.GET_AESUMES_FAILD, err })

const isResumesUpload = () => ({ type: types.IS_AESUMES_UPLOAD })


export const deleteResumes = id => ({ type: types.DELETE_AESUMES, id })
export const editResumes= (id, text) => ({ type: types.EDIT_AESUMES, id, text })

