/*
* @Author: luck-cui
* @Date:   2017-10-31 12:16:59
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-31 23:38:07
*/


import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Remarkable from 'remarkable'
import hljs from 'highlight.js'




import * as resumesActions from '../actions/resumes'


class Show extends React.Component {

  constructor(props) {
    super(props)
      // this.state={
      //   text:this.props.text.content||''
      // }
    this.md= new Remarkable('full',{
      html:         true,        // Enable html tags in source
      xhtmlOut:     false,        // Use '/' to close single tags (<br />)
      breaks:       false,        // Convert '\n' in paragraphs into <br>
      langPrefix:   'language-',  // CSS language prefix for fenced blocks
      linkify:      true,        // Autoconvert url-like texts to links
      typographer:  true,        // Enable smartypants and other sweet transforms
      linkTarget:   '', 
      quotes: '“”‘’',
      highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
          try {
            return hljs.highlight(lang, str).value;
          } catch (err) {}
        }
        try {
          return hljs.highlightAuto(str).value;
        } catch (err) {}
        return ''; // use external default escaping
      }
    });

    this.md.renderer.rules.table_open = function () {
      return '<table class="table table-striped">\n';
    };

    this.md.renderer.rules.paragraph_open = function (tokens, idx) {
      var line;
      if (tokens[idx].lines && tokens[idx].level === 0) {
        line = tokens[idx].lines[0];
        return '<p class="line" data-line="' + line + '">';
      }
      return '<p>';
    };

    this.md.renderer.rules.heading_open = function (tokens, idx) {
      var line;
      if (tokens[idx].lines && tokens[idx].level === 0) {
        line = tokens[idx].lines[0];
        return '<h' + tokens[idx].hLevel + ' class="line" data-line="' + line + '">';
      }
      return '<h' + tokens[idx].hLevel + '>';
    };
  }

  componentWillMount(){
  	const id = this.props.routeParams.id||'59f854033f89756c3d3a7606'
  	if(id) this.props.getResumes(id)
  }



  rawMarkup(){
    var rawMarkup = this.md.render(this.props.text.content||'');
    return {__html:rawMarkup};
  }
  



  componentDidMount(){ 
  	if(document.getElementById("cuijinlai"))
  	document.getElementById("cuijinlai").onclick = function(){
     var s = document.getElementById("cui")
     var printData = s.innerHTML; 
     window.document.body.innerHTML = printData;
     window.document.body.style.padding="50px 50px"
     window.print();
	}
}

  render(){
  	console.log('this.props.routeParams.id',this.props.routeParams.id=='59f854033f89756c3d3a7606')
    return (
      <div className='body'>
      <div className='center'>
      {this.props.routeParams.id?<button className= 'five' id='cuijinlai'>点击后保存即可生成pdf</button>:<div/>}
      </div>
        <section 
          
          className='show1'
          >
        <div id='cui' className='full-height' dangerouslySetInnerHTML={this.rawMarkup()}>
        </div>
        </section>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  ...state.resumes
})

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(resumesActions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Show)

